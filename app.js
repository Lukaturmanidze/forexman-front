/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'ForexMan.Application',

    name: 'ForexMan',

    requires: [
        // This will automatically load all classes in the ForexMan namespace
        // so that application classes do not need to require each other.
        'ForexMan.view.main.MainLayout',

        'ForexMan.view.main.Course',
        'ForexMan.view.main.CourseController',
        'ForexMan.view.main.ContentController',

        'ForexMan.view.trade.Trade',
        'ForexMan.view.trade.Income',
        'ForexMan.view.trade.Identification',
        'ForexMan.view.trade.IdentificationController',
        'ForexMan.view.trade.Expense',

        'ForexMan.view.main.GridRowInnerPanel',

        'ForexMan.view.ongoingOperations.Ongoing',

        'ForexMan.view.incomeExpense.IncomeExpense',
        'ForexMan.store.IncomeExpense'
    ],

    // The name of the initial view to create.
    mainView: 'ForexMan.view.main.MainLayout'
});
