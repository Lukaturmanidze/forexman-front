Ext.define('ForexMan.store.IncomeExpense', {
    extend: 'Ext.data.Store',
    //alias: 'store.incomeexpense',

    requires: [
        'Ext.data.field.Field'
    ],

    
    storeId: 'OngoingStore',
    data: [
        {
            cashNumber: 256,
            Status: 'tempora',
            type: 'dolor',
            Amount: 888.16,
            Course: 'accusamus'
        },
        {
            cashNumber: 538,
            Status: 'et',
            type: 'est',
            Amount: 910.39,
            Course: 'officiis'
        },
        {
            cashNumber: 349,
            Status: 'aut',
            type: 'inventore',
            Amount: 291.87,
            Course: 'similique'
        },
        {
            cashNumber: 618,
            Status: 'provident',
            type: 'distinctio',
            Amount: 584.88,
            Course: 'alias'
        },
        {
            cashNumber: 121,
            Status: 'magnam',
            type: 'explicabo',
            Amount: 893.14,
            Course: 'quis'
        }
    ],
    fields: [
        {
            name: 'cashNumber'
        },
        {
            name: 'Status'
        },
        {
            name: 'type'
        },
        {
            name: 'Amount'
        },
        {
            name: 'Course'
        }
    ]
});