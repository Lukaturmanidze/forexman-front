Ext.define('ForexMan.view.main.GridRowInnerPanel',{
    extend: 'Ext.panel.Panel',
    xtype: 'gridrowinnerpanel',

    layout: {
        type: 'hbox',
        align: 'stretch',
        pack: 'center'
    },
    
    defaults: {
        flex: 1,
        margin: 15,
    },

    items: [
        {
            xtype: 'displayfield',
            fieldLabel: 'Cashbox',
            value: 'cashbox1'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Consumer',
            value: 'consumer'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Customer',
            value: 'customer'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Profit',
            value: '20.53$'
        },
        {
            xtype: 'panel',
            margin: '',
            layout: {
                type: 'vbox',
                align: 'middle',
                pack: 'center'
            },

            items:[
                {
                    xtype: 'button',
                    text: 'Print',
                    handler: 'onPrint',
                    width: 120,

                    style: 'background-color: #3F51B5;',
                    margin: '10 10 5 10'
                },
                {
                    xtype: 'button',
                    text: 'Export',
                    handler: 'onExport',
                    style: 'background-color: #E8EAF6;',
                    margin: '0 10 10 10',
                    
                    width: 120,
                }
            ]
        }
    ]
})