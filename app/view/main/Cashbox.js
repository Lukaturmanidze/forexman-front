Ext.define('ForexMan.view.main.Cashbox', {
    xtype: 'cashbox',
    title: 'Cashbox',

    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.layout.container.VBox'
    ],

    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    }


})