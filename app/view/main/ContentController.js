Ext.define('ForexMan.view.main.ContentController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.content',

    tradeClick: function() {
        var card = this.lookupReference('content');
        card.setActiveItem(0);
    },

    cashboxClick: function(){
        
    },

    outGoingClick: function(){
        
    },

    onGoingClick: function(){
        var card = this.lookupReference('content');
        card.setActiveItem(3);
    },

    onOperations: function() {

    },

    onIncomeExpense: function() {
        var card = this.lookupReference('content');
        card.setActiveItem(6);
    }
})