Ext.define('ForexMan.view.main.MainLayout', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    xtype: 'mainLayout',

    controller: 'content',

    default: {
        //region: 'center',
    },

    items: [
        {
            xtype: 'panel',
            region: 'west',
            width: 250,

            items: [
                {
                    xtype: 'panel',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    defaults: {
                        textAlign: 'left'
                    },

                    items: [
                        {
                            html: 'Daily Operations',
                            bodyStyle: 'color: black;font-size: 18px',
                            margin: '10 10 15 10',
                            height: 25,
                        },
                        {
                            xtype: 'button',
                            text: 'Buy/Sell',
                            handler: 'tradeClick'
                        },
                        {
                            xtype: 'button',
                            text: 'Cashbox',
                            handler: 'cashboxClick'
                        },
                        {
                            xtype: 'button',
                            text: 'Outgoing Operations',
                            handler: 'outGoingClick'
                        },
                        {
                            xtype: 'button',
                            text: 'Ongoing Operations',
                            handler: 'onGoingClick'
                        },
                        {
                            html: 'Daily Operations',
                            bodyStyle: 'color: black;font-size: 18px',
                            margin: '10 10 15 10',
                            height: 25,
                        },
                        {
                            xtype: 'button',
                            text: 'Cashbox Remains',
                            handler: 'onCashboxremains'
                        },
                        {
                            xtype: 'button',
                            text: 'Operations',
                            handler: 'onOperations'
                        },
                        {
                            xtype: 'button',
                            text: 'Income/Expense',
                            handler: 'onIncomeExpense'
                        }
                    ]
                }
            ]
        },
        {
            reference: 'content',
            layout: 'card',

            region: 'center',

            defaults: {
                bodyStyle:{
                    backgroundColor: '#ededed'
                },
                style: {
                    backgroundColor: '#ededed'
                },
                region: 'center'
            },

            items: [
                {
                    xtype: 'trade'
                },
                {

                },
                {

                },
                {
                    xtype: 'ongoing'
                },
                {

                },
                {

                },
                {
                    xtype: 'incomeexpense'
                },
                {
                    
                }
            ]
        },
        {
            xtype: 'panel',
            region: 'east',
            width: 400,
            xtype: 'coursepanel'
        }
    ]
})