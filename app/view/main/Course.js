Ext.define('ForexMan.view.main.Course', {
    extend: 'Ext.panel.Panel',
    xtype: 'coursepanel',

    height: 1000,
    bodyPadding: 10,
    align: 'left',

    items: [
        {
            xtype: 'fieldset',
            title: 'Current Course',
            items: [
                {
                    xtype: 'grid',
                    display: 'stretch',

                    columns: [
                        {
                            flex: 1,
                            text: 'course',
                            dataIndex: 'currency'
                        },
                        {
                            width: 50,
                            text: 'X',
                            dataIndex: 'x'
                        },
                        {
                            width: 60,
                            text: 'buy',
                            dataIndex: 'buy'
                        },
                        {
                            width: 60,
                            text: 'sell',
                            dataIndex: 'sell'
                        },
                        {
                            text: 'official course',
                            dataIndex: 'officialCourse'
                        }
                    ],
                    store: {
                        storeId: 'courseStore',
                        //model: 'ForexMan.model.Currency',
                        data: [
                            { currency: 'GEL', x: '0.5', buy: '3.00', sell: '2.95', officialCourse: '3.33' }
                        ]
                    }
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Remaining Currency',

            layout: {
                type: 'vbox',
                align: 'stretch',
            },
            items: [
                {
                    xtype: 'grid',

                    controller: 'course',

                    columns: [
                        {
                            width: 80,
                            text: 'Currency',
                            dataIndex: 'currency'
                        },
                        {
                            flex: 1,
                            text: 'amount',
                            dataIndex: 'amount'
                        },
                        {
                            flex: 1,
                            text: 'value by statistics',
                            dataIndex: 'statisticsValue'
                        }
                    ],
                    store: {
                        storeId: 'store2',
                        //model: 'ForexMan.model.Remains',
                        data: [
                            { currency: 'GEL', amount: '24,949', statisticsValue: 8782 },
                            { currency: 'USD', amount: '40,256', statisticsValue: 8782 },
                            { currency: 'EUR', amount: '35,417', statisticsValue: 8782 }
                        ]
                    },

                    tbar: {
                        reference: 'statisticsSum',
                        height: 40,
                        layout: {
                            type: 'hbox',
                            align: 'stretch',
                            pack: 'center'
                        },
                        style: 'font-size: 20px;color: white; background-color:#825C99',
                        listeners: {
                            afterrender: 'calculate'
                        }
                    }
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Money on Way',

            items: [
                {
                    xtype: 'grid',
                    columns: [
                        {
                            flex: 1,
                            text: 'Currency',
                            dataIndex: 'currency'
                        }, {
                            flex: 2,
                            text: 'Person',
                            dataIndex: 'person'
                        }, {
                            flex: 1,
                            text: 'Amount',
                            dataIndex: 'amount'
                        }
                    ],
                    store: {
                        data: [
                            { currency: 'GEL', person: 'Luka', amount: '700' }
                        ]
                    }

                }

            ]
        }
    ]
})