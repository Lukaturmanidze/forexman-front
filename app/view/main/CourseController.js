Ext.define('ForexMan.view.main.CourseController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.course',

    calculate: function() {
        var sum = this.lookupReference('statisticsSum')
        sum.update(Ext.StoreManager.lookup('store2').sum('statisticsValue'))
    }
})