Ext.define('ForexMan.view.ongoingOperations.Ongoing', {
    extend: 'Ext.panel.Panel',
    xtype: 'ongoing',


    layout: {
        type: 'vbox',
        align: 'stretch'
    },


    style: {
        backgroundColor: '#ededed'
    },
    bodyStyle: {
        backgroundColor: '#ededed'
    },
    defaults:{
        margin: '30 50 15 50',
    },

    items: [
        {
            xtype: 'panel',
            height: 87,
            maxHeight: 200,
            bodyPadding: 20,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'center'
            },

            margin: '30 50 20 50',

            items: [
                {
                    flex: 1,
                    xtype: 'timefield',
                    fieldLabel: 'From',
                    labelAlign: 'top',

                    margin: '0 30 0 0'
                },
                {
                    flex:1,
                    xtype: 'timefield',
                    fieldLabel: 'Label',
                    labelAlign: 'top'
                },
                {
                    flex: 0.5
                },
                {
                    xtype: 'button',
                    flex: 1,
                    //maxWidth: 120,
                    //width: 120,
                    text: 'Filter',
                    style: 'border: 1px solid blue'
                }
            ]
        },
        {
            xtype: 'grid',
            
            margin: '10 50 10 50',
            flex: 1,
            title: '',
            //store: 'OngoingStore',
            columns: [
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    width: '',
                    dataIndex: 'cashNumber',
                    text: 'Check Number'
                },
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'status',
                    text: 'Status'
                },
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'type',
                    text: 'Type'
                },
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'amount',
                    text: 'Amount'
                },
                {
                    xtype: 'gridcolumn',
                    flex: 1,
                    dataIndex: 'course',
                    text: 'Course'
                }
            ],
            store: {
                data: [
                    {
                        cashNumber: 256,
                        status: 'tempora',
                        type: 'dolor',
                        amount: 888.16,
                        course: 'accusamus'
                    },
                    {
                        cashNumber: 538,
                        status: 'et',
                        type: 'est',
                        amount: 910.39,
                        course: 'officiis'
                    },
                    {
                        cashNumber: 349,
                        status: 'aut',
                        type: 'inventore',
                        amount: 291.87,
                        course: 'similique'
                    }
                ]
            },
            

            plugins: {
                rowwidget: {
                    widget: {
                        xtype: 'gridrowinnerpanel'
                    }
                }
            }
        }
    ]
})