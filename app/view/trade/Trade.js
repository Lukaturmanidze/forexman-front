Ext.define('ForexMan.view.trade.Trade', {
    xtype: 'trade',
    extend: 'Ext.panel.Panel',

    controller: 'trade',

    style: {
        backgroundColor: '#ededed'
    },
    bodyStyle: {
        backgroundColor: '#ededed'
    },
    defaults:{
        margin: '0 50 15 50',
        bodyPadding: '20 50 20 50'
    },
    scrollable: true,


    items: [
        {
            xtype: 'form',
            margin: '30 50 20 50',

            items:[
                {
                xtype: 'fieldcontainer',

                layout: 'hbox',
                defaults: {
                    flex: 1,
                },

                items: [
                    {
                        xtype: 'combo',
                        displayField: 'type',
                        fieldLabel: 'Operation Type',
                        height: 50,

                        store: {
                            type: 'array',
                            fields: ['type'],
                            data: [
                                ['შესყიდვა']
                            ]
                        }
                    }
                ]
            }]
            
        }, {
            xtype: 'incomeWindow'

        },{
            xtype: 'expenseWindow'
        },{
            xtype: 'panel',
            layout: 'hbox',

            defaults: {
                flex: 1
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Refresh',
                },
                {
                    flex: 3
                },
                {
                    xtype: 'button',
                    text: 'Check',
                },
                {
                    xtype: 'button',
                    text: 'Submit',
                }
            ]
        }
    ]
})