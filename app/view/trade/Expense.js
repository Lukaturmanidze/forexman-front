Ext.define('ForexMan.view.trade.Expense',{
    extend: 'Ext.panel.Panel',
    xtype: 'expenseWindow',


    layout: {
        type: 'vbox',
        align: 'stretch',
    },

    title: 'Expense',
    titleAlign: 'center',
    ui: 'light',

    items: [
        {
            xtype: 'combo',
            fieldLabel: 'Currency',
            
            width: 500,

            displayField: 'currency',
            store: {
                type: 'array',
                fields: ['currency'],
                data: [
                    ['USD']
                ]
            }
        },
        {
            xtype: 'numberfield',
            fieldLabel: 'Amount',
            
            width: 500
        }
    ]
})