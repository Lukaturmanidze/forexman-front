Ext.define('ForexMan.view.trade.Income', {
    extend: 'Ext.form.Panel',
    xtype: 'incomeWindow',


    title: 'Income',
    titleAlign: 'center',
    ui: 'light',

    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            items: [
                {
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'bottom'
                    },
                    defaults: {
                        flex: 1,
                        margin: '0 12 0 12'
                    },

                    items: [
                        {
                            flex: 5,
                            xtype: 'combo',
                            fieldLabel: 'Currency',
                            margin: '0 12 0 0',

                            displayField: 'currency',
                            store: {
                                type: 'array',
                                fields: ['currency'],
                                data: [
                                    ['USD']
                                ]
                            }
                        }, {
                            xtype: 'button',
                            text: 'GEL',
                            width: 50,
                            style: 'border: 1px solid blue'
                        }, {
                            xtype: 'button',
                            text: 'USD',
                            width: 50,
                            style: 'border: 1px solid blue'
                        }, {
                            xtype: 'button',
                            width: 50,
                            text: 'RUB',
                            style: 'border: 1px solid blue'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'bottom'
            },
            defaults: {
                flex: 1,
                margin: '0 12 0 12'
            },

            items: [
                {
                    xtype: 'numberfield',
                    flex: 5,
                    fieldLabel: 'Amount*',
                    margin: '0 12 0 0',
                    listeners: {
                        change: 'onCurrencyChange'
                    }

                }, {
                    xtype: 'button',
                    text: '10',
                    width: 50,
                    //baseCls: '',
                    style: "border: 1px solid blue;padding:''",
                }, {
                    xtype: 'button',
                    text: '25',
                    width: 50,
                    style: 'border: 1px solid blue'
                }, {
                    xtype: 'button',
                    width: 50,
                    text: '50',
                    style: 'border: 1px solid blue'
                }
            ]
        },
        {
            
            xtype: 'displayfield',
            flex: 5,

            fieldLabel: 'Course',
            value: '2.6895478',
            style: ''
        },
        {
            xtype: 'identification',
            reference: 'identity'
        }
    ]
})