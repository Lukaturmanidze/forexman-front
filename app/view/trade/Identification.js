Ext.define('ForexMan.view.trade.Identification', {
    extend: 'Ext.panel.Panel',
    xtype: 'identification',

    
    controller: 'identification',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'middle',
                pack: 'center'
            },

            items: [
                {
                    maxWidth: 200,
                    height: 40,
                    xtype: 'button',
                    text: 'Individual',
                    handler: 'onIndividual'
                },
                {
                    maxWidth: 200,
                    height: 40,
                    xtype: 'button',
                    text: 'Organization',
                    handler: 'onOrganization'
                }
            ]
        },
        {
            reference: 'identificationCard',
            xtype: 'panel',
            layout: 'card',


            defaults :{
                xtype: 'panel',
                    layout: {
                        type: 'vbox',
                        align: 'stretch',
                    },
            },

            items:[
                {
                    items: [
                        {
                            xtype: 'field',
                            allowBlank: false,
                            fieldLabel: 'Company ID',
                            width: 300
                        },
                        {
                            xtype: 'field',
                            allowBlank: false,
                            fieldLabel: 'Private ID',
                            width: 300
                        },
                        {
                            xtype: 'field',
                            allowBlank: false,
                            fieldLabel: 'Assignment',
                            width: 300
                        }
                    ]
                },
                {
                    items: [
                        {
                            xtype: 'field',
                            allowBlank: false,
                            fieldLabel: 'Private ID',
                            width: 300
                        },
                        {
                            xtype: 'field',
                            allowBlank: false,
                            fieldLabel: 'Assignment',
                            width: 300
                        }
                    ]
                }
            ]
        }
    ]
})