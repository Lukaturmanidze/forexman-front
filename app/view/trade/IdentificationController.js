Ext.define('ForexMan.view.trade.IdentificationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.identification',

    onIndividual: function(){
        debugger
        this.lookupReference('identificationCard').setActiveItem(0)
    },

    onOrganization: function() {
        this.lookupReference('identificationCard').setActiveItem(1)
    }
})