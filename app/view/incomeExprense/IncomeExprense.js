Ext.define('ForexMan.view.incomeExpense.IncomeExpense',{
    extend: 'Ext.panel.Panel',
    xtype: 'incomeexpense',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },


    style: {
        backgroundColor: '#ededed'
    },
    bodyStyle: {
        backgroundColor: '#ededed'
    },
    defaults:{
        margin: '30 50 15 50',
    },

    items:[
        {
            xtype: 'panel',
            height: 87,
            maxHeight: 200,
            bodyPadding: 20,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'center'
            },

            margin: '30 50 20 50',

            items: [
                {
                    flex: 1,
                    xtype: 'datefield',
                    fieldLabel: 'From',

                    margin: '0 30 0 0'
                },
                {
                    flex:1,
                    xtype: 'datefield',
                    fieldLabel: 'To'
                },
                {
                    flex: 0.5
                },
                {
                    flex: 1,
                    xtype: 'button',
                    text: 'Filter',
                    style: 'border: 1px solid blue'
                }
            ]
        },
        {
            xtype: 'grid',
            margin: '10 50 10 50',

            columns:[
                {
                    text: 'Type',
                    dataIndex: 'type',
                    flex: 1,
                },
                {
                    text: 'Inc/Exp',
                    dataIndex: 'inc',
                    flex: 1,
                },
                {
                    text: 'Cashbox',
                    dataIndex: 'cashbox',
                    flex: 1,
                },
                {
                    text: 'Currency',
                    dataIndex: 'currency',
                    flex: 1,
                },
            ],

            store: {
                data: [
                    {type: 'Buy', inc: 'Expense', cashbox: 'Cashbox1', currency: 'EUR'},
                    {type: 'Buy', inc: 'Expense', cashbox: 'Cashbox1', currency: 'EUR'},
                    {type: 'Buy', inc: 'Expense', cashbox: 'Cashbox1', currency: 'EUR'},
                    {type: 'Buy', inc: 'Expense', cashbox: 'Cashbox1', currency: 'EUR'},
                    {type: 'Buy', inc: 'Expense', cashbox: 'Cashbox1', currency: 'EUR'},
                ]
            },

            plugins: {
                rowwidget: {
                    widget: {
                        xtype: 'gridrowinnerpanel'
                    }
                }
            }
        }
    ]

})