Ext.define('ForexMan.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'ForexMan.model'
    }
});
