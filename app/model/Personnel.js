Ext.define('ForexMan.model.Personnel', {
    extend: 'ForexMan.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
